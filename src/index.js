import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, Link, Switch, BrowserRouter as Router } from "react-router-dom";
import Login from './layout/Login';
import AdminForgotPassword from './components/AdminLogin/AdminForgotPassword';
import About from './components/AdminLogin/About'
import Terms from './components/AdminLogin/Terms'
import AdminCreatePassword from './components/AdminLogin/AdminCreatePassword';
import NewProject from './layout/NewProject';
import DashboardProject from './layout/DashboardProject';
import DashboardFieldMember from './layout/DashboardFieldMembers';
import DashboardAdmin from './layout/DashboardAdmin';
import DashboardForm from './layout/DashboardForm';
import NewFieldMember from './layout/NewFieldMember';
import NewAdmin from './layout/NewAdmin';
import NewForm from './layout/NewForm';
import ProjectFormInfo from './components/Project/ProjectFormInfo';
import ProjectInfoSignature from './components/Project/ProjectInfoSignature';
import ProjectInfoMembers from './components/Project/ProjectInfoMembers';
import AdminProfile from './layout/AdminProfile';
import EnterNumber from './components/Web/EnterNumber';
import VerifyOtp from './components/Web/VerifyOtp';
import UserDashboard from './components/Web/UserDashboard';
import ViewForm from './components/Project/ViewForm';

const route = (
  <Router>
    <Route exact path="/" component={Login}></Route>
    <Route path="/admin/login" component={Login}></Route>
    <Route path="/admin/resetPassword" component={AdminForgotPassword}></Route>
    <Route path="/admin/setPassword" component={AdminCreatePassword}></Route>
    <Route path="/about" component={About}></Route>
    <Route path="/terms" component={Terms}></Route>
    <Route path="/dashboard/project" component={DashboardProject}></Route>
    <Route path="/dashboard/fieldmember" component={DashboardFieldMember}></Route>
    <Route path="/dashboard/admin" component={DashboardAdmin}></Route>
    <Route path="/dashboard/form" component={DashboardForm}></Route>
    <Route path="/project/create" component={NewProject}></Route>
    <Route path="/fieldmember/create" component={NewFieldMember}></Route>
    <Route path="/admin/create" component={NewAdmin}></Route>
    <Route path="/form/create" component={NewForm}></Route>
    <Route path="/projectinfoform" component={ProjectFormInfo}></Route>
    <Route path="/projectinfosignatures" component={ProjectInfoSignature}></Route>
    <Route path="/projectinfomembers" component={ProjectInfoMembers}></Route>
    <Route path="/adminprofile" component={AdminProfile}></Route>
    <Route path="/webuser" component={EnterNumber}></Route>
    <Route path="/verifyotp" component={VerifyOtp}></Route>
    <Route path="/userdashboard" component={UserDashboard}></Route>
    <Route path="/viewform" component={ViewForm}></Route>
  </Router>
);

ReactDOM.render(
  route,
  document.getElementById('root')
);
