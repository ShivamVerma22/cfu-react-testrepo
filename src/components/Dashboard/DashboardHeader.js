import React from 'react'
import '../../css-components/DashboardHeader.css'

class DashboardHeader extends React.Component {

  menuclick = (e) => {
    document.querySelector("#nav").classList.toggle("active");
    document.querySelector("#main").classList.toggle("active");
  };

  render() {
    return (
      <header className="header">
        <h1 className="header__text header__text--color">
          {this.props.pagename}
        </h1>
        <div className="header__elements">
          <div className="header__elements__search">
            <input
              className="header__elements__search__input"
              type="text"
              placeholder="Search"
            />
            <i className="ri-search-line header__elements__search__icon"></i>
          </div>
          <a
            href={"/"+this.props.pathname+"/create"}
            className="header__elements__button"
          >
            New {this.props.pagename.slice(0, -1)}
          </a>
          <i
            className="ri-menu-line header__elements__menuicon"
            id="menu"
            onClick={this.menuclick}
          ></i>
        </div>
      </header>
    );
  }
}

export default DashboardHeader;