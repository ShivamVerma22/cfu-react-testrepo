import React from "react";
import DataContainer from "../common-components/DataContainer";
import DashboardHeader from "./DashboardHeader";

function DashboardMainSectionTop({ pagename, pathname }) {
    return(
      <div className="dashboard__mainsection__top dashboard__mainsection__top--color">
        <DashboardHeader pagename={pagename} pathname={pathname}/>
      </div>
    )
}

function DashboardMainSectionBottom() {
  return(
    <div className="dashboard__mainsection__bottom dashboard__mainsection__bottom--color">
      <DataContainer />
    </div>
  )
}

function DashboardMainSection({ pagename, pathname }) {
  return (
    <div className="dashboard__mainsection" id="main">
      <DashboardMainSectionTop pagename={pagename} pathname={pathname} />
      <DashboardMainSectionBottom />
    </div>
  );
}

export default DashboardMainSection;

