import React from "react"
import "../../css-components/Profile.css";

function Profile() {
  return (
    <a href="/adminprofile" className="profile__link">
      <div className="profile">
        <div className="profile__image">
          <img
            src="https://images.unsplash.com/photo-1492562080023-ab3db95bfbce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1031&q=80"
            alt="profile image"
          />
        </div>
        <div className="profile__details">
          <h5 className="profile__details__name">Praveen CK</h5>
          <p className="profile__details__role">Admin</p>
        </div>
      </div>
    </a>
  );
}

export default Profile;