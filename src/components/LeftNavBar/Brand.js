import { render } from "@testing-library/react";
import React from "react";
import "../../css-components/Brand.css"

class Brand extends React.Component {
  menuclose = (e) => {
    document.querySelector("#nav").classList.toggle("active");
    document.querySelector("#main").classList.toggle("active");
  }
  
  render() {
    return (
      <div className="brand">
        <img
          src="../assets/img_cfu_logo_menu@2x.png"
          className="brand__img"
          alt=""
        />
        <i className="ri-close-fill navbar__link__close" id="close" onClick={this.menuclose}></i>
      </div>
    );
  }
}

export default Brand;