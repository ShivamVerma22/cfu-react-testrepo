import React from "react";
import "../../css-components/leftSideBar.css";
import Brand from "./Brand";
import LinkToDashboard from "./LinkToDashboard";

function CreatePageLeftSideBar({pagename,pathname})  {
    return (
      <div
        className="dashboard__leftnavbar dashboard__leftnavbar--color"
        id="nav"
      >
        <Brand />
        <LinkToDashboard
          pagename={pagename}
          pathname={pathname}
        />
      </div>
    );
}

export default CreatePageLeftSideBar;

