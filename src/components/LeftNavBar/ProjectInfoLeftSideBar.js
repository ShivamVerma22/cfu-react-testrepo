import React from "react";
import "../../css-components/leftSideBar.css";
import Brand from "./Brand";
import '../../css-components/ProjectInfoLeftSideBar.css'

function LinkToDashboard() {
    return (
        <a href='/dashboard/project' className="link__todashboard">
            <img src="../assets/ic_back_arrow_menu.png" alt="" />
            Projects
        </a>
    );
}

class ProjectInfoLeftSideBar extends React.Component{
    checkactive = () => {
        document.querySelectorAll(".navbarprojectinfo__link").forEach((x) => {
            if (x.classList.contains("active")) {
                x.classList.remove("active");
            }
        });

        document.querySelectorAll(".navbarprojectinfo__link").forEach((x) => {
            if (x.children[1].textContent === this.props.pagename) {
                x.classList.toggle("active");
            }
        });
    }

    componentDidMount() {
        this.checkactive();
    }

    render() {
        return (
            <div
                className="dashboard__leftnavbar dashboard__leftnavbar--color"
                id="nav"
            >
                <Brand />
                <LinkToDashboard />
                <nav className="navbarprojectinfo">
                    <h4 className="projectname">IBM Girls School</h4>
                    <ul className="navbarprojectinfo__ul">
                        <li className="navbarprojectinfo__link active">
                            <div className="navbarprojectinfo__link__icon"><p>3</p></div>
                            <a href='/projectinfoform' className="navbarprojectinfo__link__a navbarprojectinfo__link__a--color">Forms</a>
                        </li>
                        <li className="navbarprojectinfo__link">
                            <div className="navbarprojectinfo__link__icon"><p>9</p></div>
                            <a href='/projectinfosignatures' className="navbarprojectinfo__link__a navbarprojectinfo__link__a--color">Signatures</a>
                        </li>
                        <li className="navbarprojectinfo__link">
                            <div className="navbarprojectinfo__link__icon"><p>5</p></div>
                            <a href='/projectinfomembers' className="navbarprojectinfo__link__a navbarprojectinfo__link__a--color">Field team Members</a>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}

export default ProjectInfoLeftSideBar;