import React from 'react';
import '../../css-components/leftSideBar.css';
import Brand from  "./Brand"
import Profile from  "./Profile"
import Nav from  "./Nav"

function LeftSideBar( { logouthandler, pagename,extraname } ) {
      return (
        <div
          className="dashboard__leftnavbar dashboard__leftnavbar--color"
          id="nav"
        >
          <Brand />
          <Profile />
          <Nav logouthandler={logouthandler} pagename={pagename} extraname={extraname} />
        </div>
      );
}

export default LeftSideBar;


