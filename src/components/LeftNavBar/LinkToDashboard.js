import React from "react";
import "../../css-components/LinkToDashboard.css"

class LinkToDashboard extends React.Component {

    state={
        path: "/dashboard/" + this.props.pathname
    }
    render() {
        return (
          <a href={this.state.path} className="link__todashboard">
            <img src="../assets/ic_back_arrow_menu.png" alt="" />
            {this.props.pagename}
          </a>
        );
    }
}

export default LinkToDashboard;