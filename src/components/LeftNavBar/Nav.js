import React from "react";
import "../../css-components/Nav.css";

class Nav extends React.Component {
  checkactive = () => {
    document.querySelectorAll(".navbar__link").forEach((x) => {
      if (x.classList.contains("active")) {
        let url = x.children[0].getElementsByTagName("img")[0].src;
        let newurl = url.replace("active", "inactive");
        x.children[0].getElementsByTagName("img")[0].src = newurl;
        x.classList.remove("active");
      }
    });

    document.querySelectorAll(".navbar__link").forEach((x) => {
      if (x.children[1].textContent === this.props.pagename || x.children[1].textContent === this.props.extraname) {
        let url = x.children[0].getElementsByTagName("img")[0].src;
        let newurl = url.replace("inactive", "active");
        x.children[0].getElementsByTagName("img")[0].src = newurl;
        x.classList.toggle("active");
      }
          
    });
  }

  componentDidMount() {
    this.checkactive();
  }


  render() {
    return (
      <nav className="navbar">
        <ul className="navbar__ul">
          <li className="navbar__link active">
            <div className="navbar__link__icon">
              <img src="../assets/ic_projects_active.png" alt="" className="" />
            </div>
            <a
              href="/dashboard/project"
              className="navbar__link__a navbar__link__a--color"
            >
              Projects
            </a>
          </li>
          <li className="navbar__link">
            <div className="navbar__link__icon">
              <img
                src="../assets/ic_field_team_members_inactive.png"
                alt=""
                className=""
              />
            </div>
            <a
              href="/dashboard/fieldmember"
              className="navbar__link__a navbar__link__a--color"
            >
              Field team members
            </a>
          </li>
          <li className="navbar__link">
            <div className="navbar__link__icon">
              <img src="../assets/ic_admins_inactive.png" alt="" className="" />
            </div>
            <a
              href="/dashboard/admin"
              className="navbar__link__a navbar__link__a--color"
            >
              Admins
            </a>
          </li>
          <li className="navbar__link">
            <div className="navbar__link__icon">
              <img src="../assets/ic_forms_inactive.png" alt="" className="" />
            </div>
            <a
              href="/dashboard/form"
              className="navbar__link__a navbar__link__a--color"
            >
              Common Forms
            </a>
          </li>
          <li className="navbar__logout">
            <a
              href="/"
              className="navbar__link__a navbar__link__a--color"
              onClick={this.props.logouthandler}
            >
              Logout
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Nav;
