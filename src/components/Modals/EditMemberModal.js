import React from "react";
import "../../css-components/Modal.css";

function EditMemberModal() {
  return (
    <div className="modal">
      <form action="#" method="POST" className="modalform form--white">
        <h1 className="modalform__heading">Edit Field Team Member Details</h1>
        <table className="modalform__table">
          <tbody>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Full Name</label>
              </td>
              <td className="modalform__table__column2">
                <input type="text" className="modalform__input" />
              </td>
            </tr>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Mobile Number</label>
              </td>
              <td className="modalform__table__column2">
                <div className="modalforminput__mobno">
                  <select
                    className="modalform__input input--select"
                    name="code"
                  >
                    <option value="+91">+91</option>
                    <option value="+31">+31</option>
                  </select>
                  <input
                    type="text"
                    className="modalform__input input--number"
                  />
                </div>
              </td>
            </tr>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Email</label>
              </td>
              <td className="modalform__table__column2">
                <input type="text" className="modalform__input" />
              </td>
            </tr>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1 column1--photo">
                <label className="modalform__inputlabel">Photo</label>
              </td>
              <td className="modalform__table__column2 column2--photo column2--unspaced">
                <input
                  type="file"
                  className="modalform__input"
                  id="file"
                  hidden
                />
                <div className="modalform__input__photo photo--small">
                  <img
                    src="../assets/img_add_photo_avatar.png"
                    alt=""
                    className="input__photo"
                  />
                </div>
                <button className="button__addphoto" id="btnphoto">
                  Add photo
                </button>
                <button className="button__remove" id="btnphoto">
                  Remove Photo
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
      <div className="modallastrow row--edit">
        <a href="#" class="modalform__link modalform__link--color link--edit">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Save
        </a>
      </div>
    </div>
  );
}

export default EditMemberModal;
