import React from "react";
import "../../css-components/Modal.css";

function DeleteMemberModal() {
  return (
    <div className="modal">
          <h1 className="deletemodalheader">Delete selected field team members?</h1>
      <br />
      <br />
      <p className="deletemessage">
              Are you sure you want to remove selected field team members from this portal? <br />
        <br />
        Forms and projects they are working on will be safe but you will not be able to add them to new projects. <br />
        <br />
        <br />
      </p>
      <p class="red">
        <input type="checkbox" class="modal__checkbox checkbox--red" />
        Yes, I want to delete these field team members.
      </p>
      <br />
      <br />
      <div className="modallastrow row--delete">
        <a href="#" class="modalform__link modalform__link--color link--delete">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Delete
        </a>
      </div>
    </div>
  );
}

export default DeleteMemberModal;
