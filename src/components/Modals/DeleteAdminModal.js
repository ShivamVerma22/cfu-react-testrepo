import React from "react";
import "../../css-components/Modal.css";

function DeleteAdminModal() {
  return (
    <div className="modal">
      <h1 className="deletemodalheader">Delete selected admins?</h1>
      <br />
      <br />
      <p className="deletemessage">
        Are you sure you want to remove selected admins from this portal? <br />
        <br />
        Projects they are working on will not be affected. However, they will
        not be able to log in or view
        <br />
        projects anymore.
        <br />
        <br />
        <br />
      </p>
      <p class="red">
        <input type="checkbox" class="modal__checkbox checkbox--red" />
        Yes, I want to delete these admins.
      </p>
      <br />
      <br />
      <div className="modallastrow row--delete">
        <a href="#" class="modalform__link modalform__link--color link--delete">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Delete
        </a>
      </div>
    </div>
  );
}

export default DeleteAdminModal;
