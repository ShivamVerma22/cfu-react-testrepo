import React from "react";
import "../../css-components/Modal.css";

function DeleteFormModal() {
  return (
    <div className="modal">
      <h1 className="deletemodalheader">Delete selected forms?</h1>
      <br />
      <br />
      <p className="deletemessage">
        Are you sure you want to remove selected forms from this portal? <br />
        <br />
        Signatures collected using these forms are safe. However, you will not
        be able to use these forms for new
        <br />
        projects.
        <br />
        <br />
        <br />
      </p>
      <p class="red">
        <input type="checkbox" class="modal__checkbox checkbox--red" />
        Yes, I want to delete these forms.
      </p>
      <br />
      <br />
      <div className="modallastrow row--delete">
        <a href="#" class="modalform__link modalform__link--color link--delete">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Delete
        </a>
      </div>
    </div>
  );
}

export default DeleteFormModal;
