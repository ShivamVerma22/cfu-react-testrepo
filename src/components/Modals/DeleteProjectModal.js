import React from "react";
import "../../css-components/Modal.css";

function DeleteProjectModal() {
  return (
    <div className="modal">
      <h1 className="deletemodalheader">Delete this project?</h1>
      <br />
      <br />
      <p className="deletemessage">
        Once deleted, this project will be permanently removed from the CFU
        Launchpad. <br />
        <br />
        Proceed with caution <br />
        <br />
        <br />
      </p>
      <p class="red">
        <input type="checkbox" class="modal__checkbox checkbox--red" />
        Yes, I want to delete this project and related documents forever.
      </p>
      <br />
      <br />
      <div className="modallastrow row--delete">
        <a href="#" class="modalform__link modalform__link--color link--delete">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Delete
        </a>
      </div>
    </div>
  );
}

export default DeleteProjectModal;
