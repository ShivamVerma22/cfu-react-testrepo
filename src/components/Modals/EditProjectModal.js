import React from "react";
import '../../css-components/Modal.css'

function EditProjectModal() {
  return (
    <div className="modal">
      <form action="#" method="POST" className="modalform form--white">
        <h1 className="modalform__heading">Edit Project Details</h1>
        <table className="modalform__table">
          <tbody>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Name</label>
              </td>
              <td className="modalform__table__column2">
                <input type="text" className="modalform__input" />
              </td>
            </tr>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Brief</label>
              </td>
              <td className="modalform__table__column2">
                <textarea
                  name="brief"
                  className="modalform__input input--textarea"
                  rows="10"
                ></textarea>
              </td>
            </tr>
            <tr className="modalform__table__row">
              <td className="modalform__table__column1">
                <label className="modalform__inputlabel">Location</label>
              </td>
              <td className="modalform__table__column2">
                <select name="location" id="" className="modalform__input">
                  <option value="India">India</option>
                  <option value="Brazil">Brazil</option>
                </select>
              </td>
            </tr>
            <tr className="modalform__table__row row--status">
              <td className="modalform__table__column1">
                <label class="modalform__inputlabel">Status</label>
              </td>
              <td className="modalform__table__column2" colspan="">
                <div className="checkstatus">
                  <input type="checkbox" className="modal__checkbox" checked />
                  <label for="">Active</label>
                </div>
                <div className="checkstatus inactive">
                  <input type="checkbox" class="modal__checkbox" disabled />
                  <label for="">Inactive</label>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
      <div className="modallastrow row--edit">
        <a href="#" class="modalform__link modalform__link--color link--edit">
          Cancel
        </a>
        <a href="#" class="modalform__submit modalform__submit--color">
          Save
        </a>
      </div>
    </div>
  );
}

export default EditProjectModal;
