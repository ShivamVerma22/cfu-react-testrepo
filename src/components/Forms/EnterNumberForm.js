import React from "react";
import "../../css-components/LoginForms.css";
import "../../css-components/createforms.css";

function EnterNumberForm() {
    return (
      <table className="form__table">
        <tbody>
          <tr className="form__table__row">
            <td className="form__table__column1 column1--wide">
              <label className="form__inputlabel">Mobile Number</label>
            </td>
            <td className="form__table__column2">
               <div className="createforminput__mobno">
                  <select
                    className="createform__input input--select"
                    name="code"
                  >
                    <option value="+91">+91</option>
                    <option value="+31">+31</option>
                  </select>
                  <input
                    type="text"
                    className="createform__input input--number"
                  />
                </div>
            </td>
          </tr>
          <tr className="form__table__row form--terms">
          <td className="form__table__column1"></td>
          <td className="form__table__column2 column2--terms">
            <input type="checkbox" className="" />
            <p>
              I agree to the{" "}
              <a href="/terms" className="termslink">
                terms & conditions
              </a>
            </p>
          </td>
        </tr>
          <tr className="form__table__row">
            <td className="form__table__column1"></td>
            <td className="form__table__column2 column2--lastrow lastrow--lesspadding" colSpan="2">
              
              <a href="/verifyotp"
                className="form__submit form__submit--color"
              >
                Next
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    );
}

export default EnterNumberForm ;