import React from "react";

function CreateProjectFormCreateStep1() {
  return (
    <form action="#" method="POST" className="createform form--white">
      <h1 className="createform__heading">Project Details</h1>
      <table className="createform__table">
        <tbody>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Name</label>
            </td>
            <td className="createform__table__column2">
              <input type="text" className="createform__input" />
            </td>
          </tr>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Brief</label>
            </td>
            <td className="createform__table__column2">
              <textarea name="brief" className="createform__input input--textarea" rows="10"></textarea>
            </td>
          </tr>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Location</label>
            </td>
            <td className="createform__table__column2">
              <select name="location" id="" className="createform__input">
                <option value="India">India</option>
                <option value="Brazil">Brazil</option>
              </select>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  );
}

export default CreateProjectFormCreateStep1;
