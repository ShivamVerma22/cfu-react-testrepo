import React from "react";
import "../../css-components/LoginForms.css";

function AdminCreatePasswordForm() {
  return (
    <table className="form__table">
      <tbody>
        <tr className="form__table__row">
          <td className="form__table__column1 column1--wide">
            <label className="form__inputlabel">Create password</label>
          </td>
          <td className="form__table__column2 column2--small" colSpan="">
            <input type="password" className="form__input" />
          </td>
        </tr>
        <tr className="form__table__row">
          <td className="form__table__column1 column1--wide">
            <label className="form__inputlabel">Confirm password</label>
          </td>
          <td className="form__table__column2 column2--small">
            <input type="password" className="form__input" />
          </td>
        </tr>
        <tr className="form__table__row form--terms">
          <td className="form__table__column1"></td>
          <td className="form__table__column2 column2--terms">
            <input type="checkbox" className="" />
            <p>
              I agree to the{" "}
              <a href="/terms" className="termslink">
                terms & conditions
              </a>
            </p>
          </td>
        </tr>
        <tr className="form__table__row">
          <td className="form__table__column1"></td>
          <td className="form__table__column2 column2--lastrow" colSpan="2">
            <a
              href="/admin/resetPassword"
              className="form__link form__link--color"
            >
              Forgot Password?
            </a>
            <a
              href="/dashboard/project"
              className="form__submit form__submit--color"
            >
              Next
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default AdminCreatePasswordForm;
