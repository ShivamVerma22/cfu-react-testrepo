import React from "react";
import "../../css-components/createforms.css";

function LastrowGrey() {
  return (
    <div className="createlastrow">
      <button className="addmember__button">Send Invite</button>
    </div>
  );
}

function LastrowWhite() {
  return (
    <div className="createlastrow createlastrow--white">
      <button className="addmember__button">Send Invite</button>
    </div>
  );
}

function ProfilerowGrey() {
  return (
    <div className="createlastrow">
      <button className="addmember__button">Save</button>
    </div>
  );
}

function UnspacedPhotoInput() {
  return (
    <tr className="createform__table__row">
      <td className="createform__table__column1 column1--photo">
        <label className="createform__inputlabel">Photo</label>
      </td>
      <td className="createform__table__column2 column2--photo column2--unspaced">
        <input type="file" className="createform__input" id="file" hidden />
        <div className="createform__input__photo photo--small">
          <img
            src="../assets/img_add_photo_avatar.png"
            alt=""
            className="input__photo"
          />
        </div>
        <button className="button__addphoto" id="btnphoto">
          Add photo
        </button>
      </td>
    </tr>
  );
}

function SpacedPhotoInput() {
  return (
    <tr className="createform__table__row">
      <td className="createform__table__column1 column1--photo">
        <label className="createform__inputlabel">Photo</label>
      </td>
      <td className="createform__table__column2 column2--photo">
        <input type="file" className="createform__input" id="file" hidden />
        <div className="createform__input__photo photo--small">
          <img
            src="../assets/img_add_photo_avatar.png"
            alt=""
            className="input__photo"
          />
        </div>
        <button className="button__addphoto" id="btnphoto">
          Add photo
        </button>
      </td>
    </tr>
  );
}

function ProfilePhotoInput() {
  return (
    <tr className="createform__table__row">
      <td className="createform__table__column1 column1--photo">
        <label className="createform__inputlabel">Photo</label>
      </td>
      <td className="createform__table__column2 column2--photo column2--unspaced">
        <input type="file" className="createform__input" id="file" hidden />
        <div className="createform__input__photo photo--small">
          <img
            src="../assets/img_add_photo_avatar.png"
            alt=""
            className="input__photo"
          />
        </div>
        <button className="button__addphoto" id="btnphoto">
          Add photo
        </button>
        <button className="button__remove" id="btnphoto">
          Remove photo
        </button>
      </td>
    </tr>
  );
}

class CommonFormContent extends React.Component {
  render() {
    return (
      <form
        action="#"
        method="POST"
        className="createform form--white form--bottomunspaced"
      >
        <table className="createform__table">
          <tbody>
            <tr className="createform__table__row">
              <td className="createform__table__column1 column1--wide">
                <label className="createform__inputlabel">Full Name</label>
              </td>
              <td className="createform__table__column2">
                <input type="text" className="createform__input" />
              </td>
            </tr>
            <tr className="createform__table__row">
              <td className="createform__table__column1">
                <label className="createform__inputlabel">Mobile Number</label>
              </td>
              <td className="createform__table__column2">
                <div className="createforminput__mobno">
                  <select
                    className="createform__input input--select"
                    name="code"
                  >
                    <option value="+91">+91</option>
                    <option value="+31">+31</option>
                  </select>
                  <input
                    type="text"
                    className="createform__input input--number"
                  />
                </div>
              </td>
            </tr>
            <tr className="createform__table__row">
              <td className="createform__table__column1">
                <label className="createform__inputlabel">Email</label>
              </td>
              <td className="createform__table__column2">
                <input type="text" className="createform__input" />
              </td>
            </tr>
            {this.props.children}
          </tbody>
        </table>
      </form>
    );
  }
}

class AddMemberForm extends React.Component {
  render() {
    if (this.props.mode === "create") {
      return (
        <>
          <CommonFormContent>
            <UnspacedPhotoInput />
          </CommonFormContent>
          <LastrowGrey />
        </>
      );
    }
    if (this.props.mode === "profile") {
      return (
        <>
          <CommonFormContent>
            <ProfilePhotoInput />
          </CommonFormContent>
          <ProfilerowGrey />
        </>
      );
    } else {
      return (
        <>
          <CommonFormContent>
            <SpacedPhotoInput />
          </CommonFormContent>
          <LastrowWhite />
        </>
      );
    }
  }
}

export default AddMemberForm;
