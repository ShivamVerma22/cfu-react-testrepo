import React from "react";
import "../../css-components/LoginForms.css";
import "../../css-components/createforms.css";

function VerifyOtpForm() {
    return (
      <table className="form__table">
        <tbody>
          <tr className="form__table__row">
            <td className="form__table__column1 column1--wide">
              <label className="form__inputlabel">Enter OTP</label>
            </td>
            <td className="form__table__column2">
                <input
                type="text"
                className="form__input"
              />
            </td>
          </tr>
          <tr className="form__table__row">
            <td className="form__table__column1"></td>
            <td className="form__table__column2 column2--lastrow lastrow--lesspadding2" colSpan="2">
                <div className="resendotp">
                    <span className="otp__timer">60</span>
                    <a href="#" className="form__link form__link--color">Resend OTP</a>
                </div>
                <a href="/userdashboard" className="form__submit form__submit--color">Next</a>
            </td>
          </tr>
        </tbody>
      </table>
    );
}

export default VerifyOtpForm ;