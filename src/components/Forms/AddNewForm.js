import React from "react";

function CommonFormContent() {
  return(
    <form action="#" method="POST" className="createform form--white form--bottomunspaced">
      <table className="createform__table">
        <tbody>
          <tr className="createform__table__row">
            <td className="createform__table__column1 column1--wide">
              <label className="createform__inputlabel">File</label>
            </td>
            <td className="createform__table__column2">
              <input type="file" className="createform__input" id="file" hidden />
              <button className="button__addfile" id="btnfile">
                Add Document
                    </button>
            </td>
          </tr>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Form Name</label>
            </td>
            <td className="createform__table__column2">
              <input type="text" className="createform__input" />
            </td>
          </tr>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Language</label>
            </td>
            <td className="createform__table__column2">
              <input type="text" className="createform__input" />
            </td>
          </tr>
          <tr className="createform__table__row">
            <td className="createform__table__column1">
              <label className="createform__inputlabel">Client</label>
            </td>
            <td className="createform__table__column2">
              <input type="text" className="createform__input" />
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  )
}

function LastrowGrey(){
  return(
    <div className="createlastrow">
      <button className="addmember__button btn--upload inactive">Upload and Insert Fields</button>
    </div>
  )
}

function LastrowWhite(){
  return(
    <div className="createlastrow createlastrow--white">
      <button className="addmember__button">Next</button>
    </div>
  )
}

class AddNewForm extends React.Component {
  render() {
    if(this.props.mode === "create"){
      return (
        <>
          <CommonFormContent />
          <LastrowGrey />
        </>
      )
    } else {
      return(
        <>
          <CommonFormContent />
          <LastrowWhite />
        </>
      )
    }
  }
}

export default AddNewForm;
