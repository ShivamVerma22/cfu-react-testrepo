import React from "react";
import "../../css-components/LoginForms.css";

class AdminLoginForm extends React.Component {
  state = {
    email: "",
    password: "test1234"
  }

  onChangeHandler = (e) => {  
    this.setState({
      [e.target.name]: e.target.value 
    })
  }

  render() {
    return (
      <table className="form__table">
        <tbody>
          <tr className="form__table__row">
            <td className="form__table__column1">
              <label className="form__inputlabel">Email</label>
            </td>
            <td className="form__table__column2">
              <input
                type="text"
                name="email"
                className="form__input"
                onChange={this.onChangeHandler}
              />
            </td>
          </tr>
          <tr className="form__table__row">
            <td className="form__table__column1"></td>
            <td className="form__table__column2 column2--lastrow" colSpan="2">
              <a
                href="/admin/resetPassword"
                className="form__link form__link--color"
              >
                Forgot Password?
              </a>
              <a
                href="/admin/setPassword"
                className="form__submit form__submit--color"
              >
                Next
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default AdminLoginForm;
