import React from "react";
import "../../css-components/Login.css";
import PageInfo from "../common-components/PageInfo";
import VerifyOtpForm from "../Forms/VerifyOtpForm";

function SideLogo() {
  return (
    <div className="homelink">
      <a href="#">
        <img src="../assets/img_cfu_logo_login.png" alt="" />
      </a>
    </div>
  );
}

function LoginTop() {
    return (
      <div className="login__top login__top--color">
        <SideLogo />
        <PageInfo page={"verifyotp"} />
      </div>
    );
}

function LoginBottom() {
  return (
    <div className="login__bottom login__bottom--color">
        <form
        action="#"
        method="POST"
        className="form form--white"
      >
          <VerifyOtpForm />
      </form>
      <LoginFooter />
    </div>
  );
}

function FooterCopyright() {
  return (
    <div className="footer__copyright">
      <a className="footer__link footer__link--extrafade" href="#">
        All rights reserved cfu 2020
      </a>
    </div>
  );
}

function LoginFooter() {
  return (
    <footer className="footer" style={{ justifyContent: "flex-end" }}>
      <FooterCopyright />
    </footer>
  );
}

function VerifyOtp() {
  return (
    <div className="login">
      <LoginTop />
      <LoginBottom />
    </div>
  );
}

export default VerifyOtp;