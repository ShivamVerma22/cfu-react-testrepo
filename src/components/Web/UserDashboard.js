import React from "react";
import "../../css-components/Login.css";
import '../../css-components/userDashboard.css'
import PageInfo from "../common-components/PageInfo";

function SideLogo() {
  return (
    <div className="homelink homelink--userdashboard">
      <a href="#">
        <img src="../assets/img_cfu_logo_login.png" alt="" />
      </a>
    </div>
  );
}

function UserDataContainer(){
    return(
        <div className="userdatacontainer">

        </div>
    )
}

function LoginTop() {
    return (
      <div className="login__top login__top--color login__top--extends">
        <SideLogo />
        <PageInfo page={"userdashboard"} />
        <a href="/webuser" className="logout">Log out</a>
      </div>
    );
}

function LoginBottom() {
  return (
    <div className="login__bottom login__bottom--color login__bottom--extends">
        <UserDataContainer />
    </div>
  );
}

function UserDashboard() {
  return (
    <div className="login login--extend">
      <LoginTop />
      <LoginBottom />
    </div>
  );
}

export default UserDashboard;