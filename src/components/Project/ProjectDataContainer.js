import React from "react";
import '../../css-components/ProjectDataContainer.css'

function DataContainerHeading({heading}){
    return(
    <h1 className="data__heading">{heading}</h1>
    )
}

function ProjectDataContainer({heading}) {
    return (
        <div className="projectdatacontainer">
            <DataContainerHeading heading={heading} />
        </div>
    )
}

export default ProjectDataContainer;