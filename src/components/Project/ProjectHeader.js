import React from "react";
import '../../css-components/ProjectHeader.css';

class ProjectHeader extends React.Component {
    menuclick = (e) => {
        document.querySelector("#nav").classList.toggle("active");
        document.querySelector("#main").classList.toggle("active");
    }

    render() {
        if (this.props.page === 1) {
            return (
                <div className="project__header">
                    <button className="project__header__btn btn--prev btn--disabled" >
                        Previous
                    </button>
                    <h3 className="header__title">Step 1/3</h3>
                    <button className="project__header__btn btn--next" onClick={this.props.next}>
                        Next
                     </button>
                    <i className="ri-menu-line header__elements__menuicon" id="menu" onClick={this.menuclick}></i>
                </div>
            );
        }
        if (this.props.page === 2) {
            return (
                <div className="project__header">
                    <button className="project__header__btn btn--prev" onClick={this.props.prev}>
                        Previous
                    </button>
                    <h3 className="header__title">Step 2/3</h3>
                    <button className="project__header__btn btn--next" onClick={this.props.next}>
                        Next
                     </button>
                    <i className="ri-menu-line header__elements__menuicon" id="menu" onClick={this.menuclick}></i>
                </div>
            );
        }
        if (this.props.page === 3) {
            return (
                <div className="project__header">
                    <button className="project__header__btn btn--prev" onClick={this.props.prev}>
                        Previous
                    </button>
                    <h3 className="header__title">Step 3/3</h3>
                    <a href="/projectinfoform" className="project__header__btn btn--next">
                        Publish Project
                     </a>
                    <i className="ri-menu-line header__elements__menuicon" id="menu" onClick={this.menuclick}></i>
                </div>
            );
        }
    }
}

export default ProjectHeader;
