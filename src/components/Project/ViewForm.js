import React from "react"
import '../../css-components/ViewForm.css'

function ViewForm(){
    return(
        <div className="viewform">
      <div className="viewform__top">
        <a href="../project/project-info-signatures.html">
            <img
            src="../assets/ic_back_arrow_menu.png"
            alt=""
            className="viewform__top__backarrow"/>
        </a>
        <h1 className="viewform__top__heading">View Individual Consent Form</h1>
        <div className="viewform__top__formdetails">
          <div className="formdetails__member">
            <h5 className="formdetails__heading">Field Team Member Details</h5>
            <p className="formdetails__info">
              <span className="info--light">Full Name</span>
              Suresh Gopi
            </p>
            <p className="formdetails__info">
              <span className="info--light">Mobile Number</span>
              +91 999 999 9999
            </p>
          </div>
          <div className="formdetails__subject">
            <h5 className="formdetails__heading">Subject Details</h5>
            <div className="formdetails__infodiv">
              <div className="info__right">
                <p className="formdetails__info info--light">Full Name</p>
                <p className="formdetails__info info--light">Mobile Number</p>
              </div>
              <div className="infoleft">
                <p className="formdetails__info">Aditi Ramgopal</p>
                <p className="formdetails__info">+91 999 999 9999</p>
              </div>
            </div>
          </div>
          <div className="formdetails__form">
            <h5 className="formdetails__heading">Form Details</h5>
            <div className="formdetails__infodiv">
              <div className="info__right">
                <p className="formdetails__info info--light">Full Name</p>
                <p className="formdetails__info info--light">Mobile Number</p>
                <p className="formdetails__info info--light">Location</p>
                <p className="formdetails__info info--light">Date</p>
              </div>
              <div className="infoleft">
                <p className="formdetails__info">Aditi Ramgopal</p>
                <p className="formdetails__info">+91 999 999 9999</p>
                <p className="formdetails__info">Bangalore, India</p>
                <p className="formdetails__info">05/08/2020</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="viewform__bottom"></div>
    </div>
    )
}


export default ViewForm;