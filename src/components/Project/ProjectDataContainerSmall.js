import React from "react"
import '../../css-components/ProjectDataContainerSmall.css'

function ContainerHeader({dataContainerTitle}){
    return(
        <div className="datacontainersmall__header">
            <h1 className="datacontainersmall__header__title">{dataContainerTitle}</h1>
            <div className="datacontainersmall__header__search">
                <input className="search__input" type="text" placeholder="Search" />
                <i className="ri-search-line search__icon"></i>
            </div>
        </div>
    )
}

function ProjectDataContainerSmall({dataContainerTitle}) {
    return(
        <div className="datacontainersmall">
            <ContainerHeader dataContainerTitle={dataContainerTitle}/>
        </div>
    )
}

export default ProjectDataContainerSmall;