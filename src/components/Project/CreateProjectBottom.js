import React from "react";
import '../../css-components/CreateProjectBottom.css'
import ProjectDataContainer from "./ProjectDataContainer";
import ProjectAddFormContainer from "./ProjectAddFormContainer";
import ProjectDataContainerSmall from "./ProjectDataContainerSmall";

function FormandTable({page}) {
    if(page === 2){
        return (
          <div className="addmember">
            <ProjectAddFormContainer
              page={page}
              formheading={"Invite New Field Team Member"}
            />
            <ProjectDataContainerSmall
              dataContainerTitle={"Add Existing Field Team Members"}
            />
          </div>
        );
    }
    if (page === 3) {
      return (
        <div className="addmember">
          <ProjectAddFormContainer
            page={page}
            formheading={"Add New Form"}
          />
          <ProjectDataContainerSmall
            dataContainerTitle={"Add from Common Forms"}
          />
        </div>
      );
    }
}

function CreateProjectBottom({page}) {
    if(page === 2){
        return (
          <>
            <ProjectDataContainer heading={"Field Team Members"} />
            <FormandTable page={page} />
          </>
        );
    }   
    if (page === 3) {
       return (
         <>
           <ProjectDataContainer heading={"Project Forms"} />
           <FormandTable page={page} />
         </>
       );
     }
}

export default CreateProjectBottom;