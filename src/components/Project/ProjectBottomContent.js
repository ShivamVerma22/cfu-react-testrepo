import React from "react";
import CreateFormContainer from "../common-components/CreateFormContainer";
import CreateProjectBottom from "./CreateProjectBottom";

class ProjectBottomContent extends React.Component {

    render() {
        if(this.props.page === 1){
            return(
                <CreateFormContainer formheading={"Project Details"} pagename={this.props.pagename} />
            );
        }
        if (this.props.page === 2 || this.props.page === 3) {
            return (
                <CreateProjectBottom page={this.props.page}/>
            );
        }
    }
}

export default ProjectBottomContent;