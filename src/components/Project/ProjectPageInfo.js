import React from "react";
import '../../css-components/PageInfo.css'

function ProjectPageInfo({page}) {
    if (page === 1) {
        return (
            <div className="pageinfo pageinfo--project">
                <h1 className="pageinfo__title">Create New Project</h1>
                <h3 className="pageinfo__desc">Add project details here. You can do this later but<br /> procastination is a thief of time.</h3>
            </div>
        );
    }
    if (page === 2) {
        return (
            <div className="pageinfo pageinfo--project23">
                <h1 className="pageinfo__title">Add Field Team Members</h1>
                <h3 className="pageinfo__desc">Build your team here by inviting new users or adding people<br />you've already worked with.</h3>
            </div> 
        );
    }
    if (page === 3) {
        return (
            <div className="pageinfo pageinfo--project23">
                <h1 className="pageinfo__title">
                    Add Forms
                    </h1>
                <h3 className="pageinfo__desc">
                        Add forms to your project by either uploading new ones or<br />
                        sourcing them from the Cleared for Use catalogue.
                </h3>
            </div>
        );
    }
}

export default ProjectPageInfo;