import React from "react"
import '../../css-components/ProjectAddFormContainer.css'
import AddMemberForm from "../Forms/AddMemberForm"
import AddNewForm from "../Forms/AddNewForm";

function FormHeading({ formheading }) {
    return (
        <h1 className="projectaddform__heading">{formheading}</h1>
    )
}

function ProjectAddFormContainer({formheading, page}){
    if(page === 2){
        return (
          <div className="addmember__formcontainer">
            <FormHeading formheading={formheading} />
            <AddMemberForm />
          </div>
        );
    }
    if (page === 3) {
      return (
        <div className="addmember__formcontainer">
          <FormHeading formheading={formheading} />
          <AddNewForm />
        </div>
      );
    }
}

export default ProjectAddFormContainer;