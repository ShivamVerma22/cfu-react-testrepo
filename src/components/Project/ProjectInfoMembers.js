import React from "react"
import ProjectInfoLeftSideBar from "../LeftNavBar/ProjectInfoLeftSideBar"
import '../../css-components/dashboard.css'
import '../../css-components/ProjectInfo.css'
import DataContainer from "../common-components/DataContainer"

function ProjectPageInfoHeader({ menuclick }) {
    return (
        <>
            <i class="ri-menu-line header__elements__menuicon" id="menu" onClick={menuclick}></i>
            <h4 className="projectpagetitle">Collaborators</h4>
            <a href="#" className="actionlink">Add Field Team Member</a>
            <div className="projectpageinfo">
                <h1 className="projectpageinfo__title">
                    IBM Girls School | India
                    <a href="#" class="row__edit edit--iconmedium">
                        <img src="../assets/ic_edit_project_details.png" alt="" />
                    </a>
                </h1>
                <h3 className="projectpageinfo__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi, minima <br /> Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere, voluptas</h3>
            </div>
        </>
    )
}

class ProjectInfoMembers extends React.Component {
    state = {
        pagename: "Field team Members",
    }

    menuclick = (e) => {
        document.querySelector("#nav").classList.toggle("active");
        document.querySelector("#main").classList.toggle("active");
    };

    render() {
        return (
            <div className="dashboard">
                <ProjectInfoLeftSideBar pagename={this.state.pagename} />
                <div className="dashboard__mainsection" id="main">
                    <div className="dashboard__mainsection__top dashboard__mainsection__top--color">
                        <ProjectPageInfoHeader menuclick={this.menuclick} />
                    </div>
                    <div className="dashboard__mainsection__bottom dashboard__mainsection__bottom--color bottom--projectinfo">
                        <DataContainer />
                    </div>
                </div>
            </div>
        )
    }
}

export default ProjectInfoMembers;