import React from "react";
import AddMemberForm from "../Forms/AddMemberForm";
import AddNewForm from "../Forms/AddNewForm";
import CreateProjectFormStep1 from "../Forms/CreateProjectFormStep1";
import '../../css-components/createforms.css'

function CreateFormContainer({ pagename }) {
  if (pagename === "Projects") {
    return (
      <div className="create">
        <CreateProjectFormStep1 />
      </div>
    );
  }
  if (pagename === "Field team members") {
    return (
      <div className="create create--topdown">
        <AddMemberForm mode={"create"} />
      </div>
    );
  }
  if (pagename === "Admins") {
    return (
      <div className="create create--topdown">
        <AddMemberForm mode={"create"} />
      </div>
    );
  }
  if (pagename === "Forms") {
    return (
      <div className="create create--topdown">
        <AddNewForm mode={"create"} />
      </div>
    );
  }
  if (pagename === "Profile") {
    return (
      <div className="create create--topdown">
        <AddMemberForm mode={"profile"} />
      </div>
    );
  }
}

export default CreateFormContainer;
