import React from "react"

function DataTable() {
    return (
      <table className="data__table" cellSpacing="0" cellPadding="0">
        <thead>
          <tr className="data__table__header">
            <th className="header__column header__column--one">
              Select
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--two">
              Last active
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--three">
              Project name
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--four">
              location
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--five">
              Status
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--six">
              Field team members
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--seven">
              Forms
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--eight">
              Signatures
              <img
                src="../assets/ic_filter_selector.png"
                alt=""
                className="header__filter"
              />
            </th>
            <th className="header__column header__column--nine">Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
          <tr className="data__table__row">
            <td className="row__column row__column--one">
              <input type="checkbox" className="row__checkbox" />
            </td>
            <td className="row__column row__column--two">
              2:00 PM | Aug 5,2020
            </td>
            <td className="row__column row__column--three">
              <a href="project-info-forms.html" className="projectlink">
                IBM Girls School Research
              </a>
            </td>
            <td className="row__column row__column--four">India</td>
            <td className="row__column row__column--five">Active</td>
            <td className="row__column row__column--six">4</td>
            <td className="row__column row__column--seven">2</td>
            <td className="row__column row__column--eight">12</td>
            <td className="row__column row__column--nine">
              <img src="../assets/ic_edit.png" alt="" className="row__edit" />
              <img
                src="../assets/ic_delete.png"
                alt=""
                className="row__delete"
              />
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr className="data__table__footer">
            <td className="footer__column footer__column--one" colSpan="2">
              <button className="footer__buttonprev">Previous</button>
            </td>
            <td className="footer__column footer__column--two" colSpan="2">
              <select name="page" id="" className="footer__pageselect" disabled>
                <option value="one">Page 1 of 10</option>
              </select>
            </td>
            <td className="footer__column footer__column--three" colSpan="2">
              <select name="rows" id="" className="footer__rowselect" disabled>
                <option value="10">10 Rows</option>
              </select>
            </td>
            <td className="footer__column footer__column--four" colSpan="3">
              <button className="footer__buttonnext active">Next</button>
            </td>
          </tr>
        </tfoot>
      </table>
    );
}