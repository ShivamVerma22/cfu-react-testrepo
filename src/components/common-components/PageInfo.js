import React from "react";
import '../../css-components/PageInfo.css'

class PageInfo extends React.Component {
  menuclick = (e) => {
    document.querySelector("#nav").classList.toggle("active");
    document.querySelector("#main").classList.toggle("active");
  };

  render() {
    if (this.props.page === "Field team members") {
      return (
        <div className="pageinfo pageinfo--down">
          <h1 className="pageinfo__title">Invite New Field Team Member</h1>
          <h3 className="pageinfo__desc">
            Enter a new field team member's detail below
            <br />
            and we will send them a invite to Cleared for use
          </h3>
          <i
            className="ri-menu-line header__elements__menuiconxx"
            id="menu"
            onClick={this.menuclick}
          ></i>
        </div>
      );
    }
    if (this.props.page === "Admins") {
      return (
        <div className="pageinfo pageinfo--down">
          <h1 className="pageinfo__title">Invite New Admin</h1>
          <h3 className="pageinfo__desc">
            Enter a new admin detail below
            <br />
            and we will send them an invite to Cleared for use
          </h3>
          <i
            className="ri-menu-line header__elements__menuiconxx"
            id="menu"
            onClick={this.menuclick}
          ></i>
        </div>
      );
    }
    if (this.props.page === "Forms") {
      return (
        <div className="pageinfo pageinfo--down">
          <h1 className="pageinfo__title">Add New Form</h1>
          <h3 className="pageinfo__desc">
            Upload a new form to your project
            <br />
            and highlight the fields which must be completed
          </h3>
          <i
            className="ri-menu-line header__elements__menuiconxx"
            id="menu"
            onClick={this.menuclick}
          ></i>
        </div>
      );
    }
    if (this.props.page === "Profile") {
      return (
        <div className="pageinfo pageinfo--down">
          <h1 className="pageinfo__title">Your Profile</h1>
          <h3 className="pageinfo__desc">
            Add personal details to your profile
          </h3>
          <i
            className="ri-menu-line header__elements__menuiconxx"
            id="menu"
            onClick={this.menuclick}
          ></i>
        </div>
      );
    }
    if (this.props.page === "enternumber") {
      return (
        <div className="pageinfo pageinfo--web">
          <h1 className="pageinfo__title">View your Signature</h1>
          <h3 className="pageinfo__desc">
             If you've signed in via Cleared for Use before, you can access your signature here. 
             <br />
              Enter your registered mobile phone number and follow the prompts.
          </h3>
        </div>
      );
    }
    if (this.props.page === "verifyotp") {
      return (
        <div className="pageinfo pageinfo--web">
          <h1 className="pageinfo__title">Verify Your Mobile Number</h1>
          <h3 className="pageinfo__desc">
             Enter the OTP sent to your registered mobile phone number.
          </h3>
        </div>
      );
    }
     if (this.props.page === "userdashboard") {
      return (
        <div className="pageinfo pageinfo--userdashboard">
          <h1 className="pageinfo__title">Welcome, Aditi</h1>
          <h3 className="pageinfo__desc">
              You can see the forms you've signed via Cleared for Use below.
              <br />
              Please contact this app's registered user if you have any questions.
          </h3>
        </div>
      );
    }
  }
}

export default PageInfo;