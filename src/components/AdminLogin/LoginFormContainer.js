import React from "react";
import AdminCreatePassword from "../components/AdminLogin/AdminCreatePassword";
import AdminLoginForm from "../components/AdminLogin/AdminLoginForm";
import AdminForgotPassword from "../components/AdminLogin/AdminForgotPassword";
import InfoText from '../components/AdminLogin/InfoText'

function InfoContainer({ page }) {
    return (
        <div className="infocontainer">
            <InfoText page={page} />
        </div>
    )
}


class LoginFormContainer extends React.Component {

    handleFormSubmit = (e) => {
        e.preventDefault();
    }

    render(){
        if (this.props.page === "login") {
            return (
                <form
                    action="#"
                    method="POST"
                    className="form form--white"
                    style={{ height: "200px" }}
                    onSubmit={this.handleFormSubmit}
                >
                    <AdminLoginForm forgot={this.props.forgot} create={this.props.create} />
                </form>
            );
        }
        if (this.props.page === "forgot") {
            return (
                <form
                    action="#"
                    method="POST"
                    className="form form--white"
                    style={{ height: "200px" }}
                    onSubmit={this.handleFormSubmit}
                >
                    <AdminForgotPassword forgot={this.props.forgot} create={this.props.create} />
                </form>
            );
        }
        if (this.props.page === "create") {
            return (
                <form
                    action="#"
                    method="POST"
                    className="form form--white"
                    style={{ height: "300px" }}
                    onSubmit={this.handleFormSubmit}
                >
                    <AdminCreatePassword forgot={this.props.forgot} login={this.props.login} terms={this.props.terms} />
                </form>
            );
        }
        if (this.props.page === "about" || this.props.page === "terms") {
            return (
                <InfoContainer page={this.props.page} />
            );
        }
    }
}

export default LoginFormContainer;