import React from "react";
import InfoText from "./InfoText";
import "../../css-components/Login.css";

function SideLogo() {
  return (
    <div className="homelink">
      <a href="/">
        <img src="../assets/img_cfu_logo_login.png" alt="" />
      </a>
    </div>
  );
}

function LoginMessage() {
  return <h4 className="pagetitle">About Us</h4>;
}

function LoginTop() {
  return (
    <div className="login__top login__top--color">
      <SideLogo />
      <LoginMessage />
    </div>
  );
}

function InfoContainer({ page }) {
  return (
    <div className="infocontainer">
      <InfoText page={page} />
    </div>
  );
}

function LoginFormContainer() {
  return <InfoContainer page={"about"} />;
}

function LoginBottom() {
  return (
    <div className="login__bottom login__bottom--color">
      <LoginFormContainer />
      <LoginFooter />
    </div>
  );
}

function FooterCopyright() {
  return (
    <div className="footer__copyright">
      <a className="footer__link footer__link--extrafade" href="#">
        All rights reserved cfu 2020
      </a>
    </div>
  );
}

function LoginFooter() {
  return (
    <footer className="footer" style={{ justifyContent: "flex-end" }}>
      <FooterCopyright />
    </footer>
  );
}

function About() {
  return (
    <div className="login">
      <LoginTop />
      <LoginBottom />
    </div>
  );
}

export default About;
