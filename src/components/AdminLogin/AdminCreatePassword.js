import React from "react";
import AdminCreatePasswordForm from "../Forms/AdminCreatePasswordForm";
import "../../css-components/Login.css";


function CenterLogo() {
  return (
    <div className="logo">
      <img src="../assets/img_cfu_logo_login.png" alt="" />
    </div>
  );
}

function LoginMessage() {
  return (
    <h4 className="login__message login__message--white login__message--faded">
      Your first time here?
      <br />
      Follow the prompts below
    </h4>
  );
}

function LoginTop() {
  return (
    <div className="login__top login__top--color">
      <CenterLogo />
      <LoginMessage />
    </div>
  );
}

function LoginFormContainer() {
  return (
    <form
      action="#"
      method="POST"
      className="form form--white"
    >
      <AdminCreatePasswordForm />
    </form>
  );
}

function LoginBottom() {
  return (
    <div className="login__bottom login__bottom--color">
      <LoginFormContainer />
      <LoginFooter />
    </div>
  );
}

function FooterRightLinks() {
  return (
    <div className="footer__rightlinks">
      <a className="footer__link footer__link--fade" href="/about">
        About us
      </a>
      <a className="footer__link footer__link--fade" href="/terms">
        Terms & Conditions
      </a>
    </div>
  );
}

function FooterCopyright() {
  return (
    <div className="footer__copyright">
      <a className="footer__link footer__link--extrafade" href="#">
        All rights reserved cfu 2020
      </a>
    </div>
  );
}

function LoginFooter() {
  return (
    <footer className="footer">
      <FooterRightLinks />
      <FooterCopyright />
    </footer>
  );
}

function AdminCreatePassword() {
  return (
    <div className="login">
      <LoginTop />
      <LoginBottom />
    </div>
  );
}

export default AdminCreatePassword;
