import React from "react";

function InfoText({ page }) {
  if (page === "about") {
    return (
      <p className="aboutustext">
        Hi!
        <br />
        <br />
        Welcome to Cleared for Use.
        <br />
        <br />
        We get it. You want on-the-go form filling and mobile signing processes
        to be more efficient,
        <br />
        safer and joyful.
        <br />
        <br />
        Our launchpad makes this happen by curating all of your signing tasks,
        templates and <br />
        projects in one place.
        <br />
        <br />
        With everything organised simply and neatly, you can deploy team members
        with the <br /> paperwork they need within minutes.
        <br />
        <br />
        Goodbye stacks of forms, clipboards and messy pens. Hello secure mobile
        signing.
        <br />
        <br />
        Organising paperwork has never been so much fun.
      </p>
    );
  }
  if (page === "terms") {
    return (
      <p className="aboutustext">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore impedit
        atque deserunt laboriosam? Temporibus accusamus alias doloremque
        laudantium assumenda vel facere excepturi explicabo veritatis nesciunt
        incidunt perspiciatis nobis ea molestias officia id quam amet harum
        expedita, animi laboriosam inventore magnam obcaecati. Veniam quis
        assumenda aspernatur nulla est. Dignissimos repellat non delectus ullam
        velit, quas corrupti libero voluptas aliquid ducimus, animi nulla
        provident. Numquam ab minus voluptatibus magnam ipsum ipsam temporibus
        aspernatur fuga itaque, consequatur vitae possimus laudantium dicta unde
        quasi modi architecto tempore voluptas hic reprehenderit sunt totam odit
        recusandae suscipit? Culpa error debitis blanditiis nam accusantium
        illum eligendi laboriosam. <br />
        <br />
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis
        omnis cupiditate harum. Eum esse atque similique, veniam laborum
        expedita fugit cum qui distinctio illo quae totam mollitia sapiente
        doloremque tenetur, repudiandae repellat magni ducimus quas alias
        voluptatem. Quas vitae porro nulla repellat atque, facilis expedita
        numquam, ad repudiandae perferendis, sint quisquam quaerat tenetur
        incidunt! Harum eveniet, magnam tenetur eos incidunt fugit aut deleniti
        quod ducimus sint. Ipsa similique voluptas, earum numquam voluptatem sed
        iusto saepe, necessitatibus accusamus non incidunt. Incidunt deleniti
      </p>
    );
  }
}

export default InfoText;