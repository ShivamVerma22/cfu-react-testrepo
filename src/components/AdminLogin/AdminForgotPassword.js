import React from "react";
import AdminForgotPasswordForm from "../Forms/AdminForgotPasswordForm";
import "../../css-components/Login.css";


function CenterLogo() {
  return (
    <div className="logo">
      <img src="../assets/img_cfu_logo_login.png" alt="" />
    </div>
  );
}

function LoginMessage() {
    return (
      <h4 className="login__message login__message--white login__message--faded">
        Recover your account
        <br />A link will be sent to your registered Email
      </h4>
    );
}

function LoginTop() {
    return (
      <div className="login__top login__top--color">
        <CenterLogo />
        <LoginMessage />
      </div>
    );
}

function LoginFormContainer() {
    return (
      <form
        action="#"
        method="POST"
        className="form form--white"
      >
        <AdminForgotPasswordForm />
      </form>
    );
}

function LoginBottom() {
  return (
    <div className="login__bottom login__bottom--color">
      <LoginFormContainer />
      <LoginFooter />
    </div>
  );
}

function FooterRightLinks() {
  return (
    <div className="footer__rightlinks">
      <a className="footer__link footer__link--fade" href="/about">
        About us
      </a>
      <a className="footer__link footer__link--fade" href="/terms">
        Terms & Conditions
      </a>
    </div>
  );
}

function FooterCopyright() {
  return (
    <div className="footer__copyright">
      <a className="footer__link footer__link--extrafade" href="#">
        All rights reserved cfu 2020
      </a>
    </div>
  );
}

function LoginFooter() {
    return (
    <footer className="footer">
      <FooterRightLinks />
      <FooterCopyright />
    </footer>
  );
}

function AdminForgotPassword() {
    return (
        <div className="login">
          <LoginTop  />
          <LoginBottom />
        </div>
      );
}

export default AdminForgotPassword;
