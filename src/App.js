import React from 'react';
import './App.css';
import Dashboard from './layout/DashboardProject';
import Login from './layout/Login'

class App extends React.Component {
  state = {
    isLoggedIn: "false",
  };

  render() {
    if (this.state.isLoggedIn === "false") {
      return <Login />;
    } else {
      return <Dashboard />;
    }
  }
}

export default App;
