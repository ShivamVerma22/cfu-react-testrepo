import React from 'react';
import '../css-components/dashboard.css';
import LeftSideBar from '../components/LeftNavBar/LeftSideBar';
import DashboardMainSection from '../components/Dashboard/DashboardMainSection'
import Login from './Login';

class DashboardProject extends React.Component {
  state = {
    pageName: "Projects",
    pathname: "project",
    isLoggedIn: "true",
  };

  logoutclick = () => {
    this.setState({
      isLoggedIn: "false",
    });
  };

  // getdata = () => {
  //   let token =
  //     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
  //   var bearer = "Bearer " + token;
  //   console.log(bearer);

  //   fetch("http://40.117.77.142:4000/project/list", {
  //     method: "GET",
  //     headers: {
  //       Authorization: bearer,
  //       "Content-Type": "application/json",
  //     },
  //     credentials: "same-origin",
  //   }).then((response) => {
  //     console.log(response);
  //   });
  // };

  // componentDidMount() {
  //   this.getdata();
  // }

  render() {
    if (this.state.isLoggedIn === "true") {
      return (
        <div className="dashboard">
          <LeftSideBar
            pagename={this.state.pageName}
            pathname={this.state.pathname}
            logouthandler={this.logoutclick}
          />
          <DashboardMainSection
            pagename={this.state.pageName}
            pathname={this.state.pathname}
          />
        </div>
      );
    } else {
      return <Login />;
    }
  }
}

export default DashboardProject;