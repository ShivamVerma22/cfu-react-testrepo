import React from "react";
import CreateFormContainer from "../components/common-components/CreateFormContainer";
import PageInfo from "../components/common-components/PageInfo";
import "../css-components/dashboard.css";
import CreatePageLeftSideBar from "../components/LeftNavBar/CreatePageLeftSideBar";

class NewFieldMember extends React.Component {
  state = {
    pageName: "Field team members",
    pathname: "fieldmember",
  };

  render() {
    return (
      <div className="dashboard">
        <CreatePageLeftSideBar
          pagename={this.state.pageName}
          pathname={this.state.pathname}
        />
        <div className="dashboard__mainsection" id="main">
          <div className="dashboard__mainsection__top dashboard__mainsection__top--color top--create">
            <PageInfo page={this.state.pageName} />
          </div>
          <div className="dashboard__mainsection__bottom dashboard__mainsection__bottom--color bottom--create">
            <CreateFormContainer pagename={this.state.pageName} />
          </div>
        </div>
      </div>
    );
  }
}

export default NewFieldMember;