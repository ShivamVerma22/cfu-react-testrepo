import React from "react";
import "../css-components/Login.css";
import AdminLoginForm from "../components/Forms/AdminLoginForm";
import { Redirect } from "react-router-dom";

function CenterLogo() {
  return (
    <div className="logo">
      <img src="../assets/img_cfu_logo_login.png" alt="" />
    </div>
  );
}

function LoginMessage() {
    return (
      <h4 className="login__message login__message--white login__message--faded">
        Hi, Admin!
        <br />
        Log in below to create and deploy paperwork for your projects.
      </h4>
    );
}

function LoginTop() {
    return (
    <div className="login__top login__top--color">
      <CenterLogo />
      <LoginMessage />
    </div>
  );
}

function LoginFormContainer({login, handleFormSubmit }) {
    return (
      <form
        action="#"
        method="POST"
        className="form form--white"
        onSubmit={handleFormSubmit}
      >
        <AdminLoginForm login={login} handleFormSubmit={handleFormSubmit}/>
      </form>
    );
}

function LoginBottom({login, handleFormSubmit}) {
  return (
    <div className="login__bottom login__bottom--color">
      <LoginFormContainer login={login} handleFormSubmit={handleFormSubmit} />
      <LoginFooter />
    </div>
  );
}

function FooterRightLinks() {
  return (
    <div className="footer__rightlinks">
      <a className="footer__link footer__link--fade" href="/about" >
        About us
      </a>
      <a className="footer__link footer__link--fade" href="/terms" >
        Terms & Conditions
      </a>
    </div>
  );
}

function FooterCopyright() {
  return (
    <div className="footer__copyright">
      <a className="footer__link footer__link--extrafade" href="#">
        All rights reserved cfu 2020
      </a>
    </div>
  );
}

function LoginFooter() {
    return (
    <footer className="footer">
      <FooterRightLinks />
      <FooterCopyright />
    </footer>
  );
}

class Login extends React.Component {
  state = {
    pageName: "login",
    isLoggedIn: "false",
  };

  login = () => {
    this.setState({
      isLoggedIn: "true",
    });
  };

  handleFormSubmit = (e) => {
    e.preventDefault();
  }

  render() {
    if (this.state.isLoggedIn === "false") {
      return (
        <div className="login">
          <LoginTop />
          <LoginBottom
            login={this.login}
            handleFormSubmit={this.handleFormSubmit}
          />
        </div>
      );
    } else {
      return <Redirect to="dashboard/project" />;
    }
  }
}

export default Login;
