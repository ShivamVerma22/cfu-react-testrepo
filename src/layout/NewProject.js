import React from "react";
import ProjectHeader from "../components/Project/ProjectHeader";
import ProjectPageInfo from "../components/Project/ProjectPageInfo";
import ProjectBottomContent from "../components/Project/ProjectBottomContent"
import '../css-components/dashboard.css'
import CreatePageLeftSideBar from "../components/LeftNavBar/CreatePageLeftSideBar";

class NewProject extends React.Component {
    state = {
        page: 1,
        pageName: "Projects",
        pathname: "project"
    }

    changePageNext = (e) => {
        e.preventDefault();
        if (this.state.page <= 3 && this.state.page >= 1){
            this.setState({
                page: this.state.page + 1
            })
        }
    }

    changePagePrev = (e) => {
        e.preventDefault();
        if (this.state.page <= 3) {
            this.setState({
                page: this.state.page - 1
            })
        }
    }

    render() {
        return(
            <div className="dashboard">
                <CreatePageLeftSideBar pagename={this.state.pageName} pathname={this.state.pathname} />
                <div className="dashboard__mainsection" id="main">
                    <div className="dashboard__mainsection__top dashboard__mainsection__top--color top--create">
                        <ProjectHeader prev={this.changePagePrev} next={this.changePageNext} page={this.state.page} />
                        <ProjectPageInfo page={this.state.page} />
                    </div>
                    <div className="dashboard__mainsection__bottom dashboard__mainsection__bottom--color bottom--create">
                        <ProjectBottomContent page={this.state.page} pagename={this.state.pageName} />
                    </div>
                </div>
            </div>
        )
    }
}

export default NewProject;