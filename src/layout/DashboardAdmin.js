import React from "react";
import "../css-components/dashboard.css";
import LeftSideBar from "../components/LeftNavBar/LeftSideBar";
import DashboardMainSection from "../components/Dashboard/DashboardMainSection";
import Login from "./Login";

class DashboardAdmin extends React.Component {
  state = {
    pageName: "Admins",
    pathname: "admin",
    isLoggedIn: "true",
  };

  logoutclick = () => {
    this.setState({
      isLoggedIn: "false",
    });
  };

  render() {
    if (this.state.isLoggedIn === "true") {
      return (
        <div className="dashboard">
          <LeftSideBar
            pagename={this.state.pageName}
            logouthandler={this.logoutclick}
            pathname={this.state.pathname}
          />
          <DashboardMainSection
            pagename={this.state.pageName}
            pathname={this.state.pathname}
          />
        </div>
      );
    } else {
      return <Login />;
    }
  }
}

export default DashboardAdmin;
