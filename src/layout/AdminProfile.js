import React from "react";
import CreateFormContainer from "../components/common-components/CreateFormContainer";
import PageInfo from "../components/common-components/PageInfo";
import "../css-components/dashboard.css";
import CreatePageLeftSideBar from "../components/LeftNavBar/CreatePageLeftSideBar";

class NewAdmin extends React.Component {
  render() {
    return (
      <div className="dashboard">
        <CreatePageLeftSideBar pagename={"Projects"} pathname={"project"} />
        <div className="dashboard__mainsection" id="main">
          <div className="dashboard__mainsection__top dashboard__mainsection__top--color top--create">
            <PageInfo page={"Profile"} />
          </div>
          <div className="dashboard__mainsection__bottom dashboard__mainsection__bottom--color bottom--create">
            <CreateFormContainer pagename={"Profile"} />
          </div>
        </div>
      </div>
    );
  }
}

export default NewAdmin;
