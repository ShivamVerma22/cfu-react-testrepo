import React from "react";
import "../css-components/dashboard.css";
import LeftSideBar from "../components/LeftNavBar/LeftSideBar";
import DashboardMainSection from "../components/Dashboard/DashboardMainSection";
import Login from "./Login";

class DashboardForm extends React.Component {
  state = {
    pageName: "Forms",
    extraname: "Common Forms",
    pathname: "form",
    isLoggedIn: "true",
  };

  logoutclick = () => {
    this.setState({
      isLoggedIn: "false",
    });
  };

  render() {
    if (this.state.isLoggedIn === "true") {
      return (
        <div className="dashboard">
          <LeftSideBar
            pagename={this.state.pageName}
            extraname={this.state.extraname}
            logouthandler={this.logoutclick}
          />
          <DashboardMainSection
            pagename={this.state.pageName}
            pathname={this.state.pathname}
          />
        </div>
      );
    } else {
      return <Login />;
    }
  }
}

export default DashboardForm;
